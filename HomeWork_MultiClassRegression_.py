from torchvision.datasets import MNIST
import matplotlib.pyplot as plt
import numpy as np
import torch

def sigmoid(z):
    return 1 / (1 + np.exp(-z))

def score(classes, weight, x, y):
    return (predict(classes,weight, x) == y).mean()


def fit_binary(X, yr, a=0.001):
    weigt = np.zeros((X.shape[1],1))
    yr = yr[:,None]
    for i in range(1000):
        y = sigmoid(X@weight)
        weight = weight + a*(X.T@(y_r-y))
    return weight

def cost(weight, x, y):
    y = y[:,None]
    n = len(y)
    sgm = sigmoid(x @ weight)
    der = 1 / n * (x.T@(y - sgm))
    return der
def predict(classes, weights, x):
    preds = [np.argmax(
        [sigmoid(xi @ weight) for weights in thetas]
    ) for xi in x]
    return [classes[p] for p in preds]
    
def fit_multcl(x, y, a=0.001):
    weights = []
    classes = np.unique(y)
    costs = np.zeros(1000)

    for c in classes:
        binary_y = np.where(y == c, 1, 0)
        
        weights = np.zeros((x.shape[1],1))
        for i in range(1000):
            der = cost(weights, x, binary_y)
            theta += a * der
            
        weights.append(weight)
    return weights, classes, costs
    


def precision_score(threshold,y,yr):
    y = y.reshape(len(y))
    P = y>=threshold
    TP = yr[P].sum()
    return TP/P.sum()

def recall_score(threshold,y,yr):
    y = y.reshape(len(y))
    P = y>=threshold
    TP = yr[y>=threshold].sum()
    FN = (yr[P<threshold]).sum()
    return TP/(TP+FN)

dataset = MNIST('../data',train=True,download=True)
X = np.array(dataset.train_data)
X = X.reshape((X.shape[0],784))
y_r = np.array(dataset.train_labels)
X_bin = X[y_r<=1]
y_r_bin = y_r[y_r<=1]

weight_bin = fit_binary(X_bin,y_r_bin)
y = X_bin@weight_bin
threshold = 0.5
y_p_bin = (y>=threshold)+0
precision = precision_score(threshold,y,y_r_bin)
print(f'Precision : {precision}')
recall = recall_score(threshold,y,y_r_bin)
print(f'Recall : {recall}')

#Multiclass logistic regression
weights, classes, costs =fit_multcl(X,yr,1000,0.0001)
accuracy = score(classes, thetas, X, yr)
print(f'Accuracy: {accuracy}')


